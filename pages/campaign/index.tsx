import React from 'react'
import CampaignCard from '../../components/campaign/campaign-card'
import Layout from '../../components/common/Layout'
import {findAll} from '../../db/campaign-repository'
import {Campaign} from '../../db/models'

type CampaignPageProps = {
  campaigns: Campaign[]
}

export default function CampaignPage({campaigns}: CampaignPageProps) {
  return (
      <Layout>
        <div id="campaigns" className="container is-fluid" style={{minHeight: "100%"}}>
          <section className="section">
            <div className="container">
              <h1 className="title is-1">Campaigns</h1>
              <div className="box">
                <p className="content">
                  Are you looking to try out new software? Are you interested
                  in assisting developers by providing feedback? Find current
                  user testing campaigns here!
                </p>
              </div>
            </div>
          </section>

          <div className="columns" id="filters">
            <div className="column is-2-fullhd is-3-widescreen is-4-desktop">
              <div className="box">
                <div className="panel">
                  <div className="panel-heading">
                    <h2 className="subtitle is-3">Filters</h2>
                  </div>
                  <p className="panel-tabs">
                    <a className="is-active">Category</a>
                    <a>Platforms</a>
                    <a>Projects</a>
                    <a>My system</a>
                  </p>
                  <div className="panel-block">
                    <fieldset name="application-type">
                      <legend>
                        <h4 className="subtitle is-4">Software type</h4>
                      </legend>
                      <div className="field">
                        <input type="checkbox" className="is-checkradio filter-tag-controller"
                               name="tag-software-desktop" id="tag-software-desktop" checked="checked"/>
                        <label htmlFor="tag-software-desktop">
                          Desktop applications
                        </label>
                      </div>
                      <div className="field">
                        <div className="control">
                          <input type="checkbox" className="is-checkradio filter-tag-controller"
                                 name="tag-software-mobile" id="tag-software-mobile"
                                 checked="checked"/>
                          <label htmlFor="tag-software-mobile">Mobile applications</label>
                        </div>
                      </div>
                      <div className="field">
                        <input type="checkbox" className="is-checkradio filter-tag-controller"
                               name="tag-software-os" id="tag-software-os" checked="checked"/>
                        <label htmlFor="tag-software-os">Operating systems</label>
                      </div>
                      <div className="field">
                        <input type="checkbox" className="is-checkradio filter-tag-controller"
                               name="tag-software-website" id="tag-software-website" checked="checked"/>
                        <label htmlFor="tag-software-website">Websites</label>
                      </div>
                    </fieldset>
                  </div>
                  <div className="panel-block">
                    <div className="field has-addons">
                      <div className="control is-expanded">
                        <div className="select is-info">
                          <select className="is-fullwidth">
                            <option>Select dropdown 2</option>
                            <option>With options</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="panel-block">
                    <div className="field">
                      <div className="control">
                        <div className="select is-success">
                          <select>
                            <option>Select dropdown</option>
                            <option>With options</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="panel-block">
                    <div className="field">
                      <div className="control">
                        <div className="select is-warning">
                          <select>
                            <option>Select dropdown</option>
                            <option>With options</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="panel-block">
                    <div className="field">
                      <div className="control">
                        <div className="select is-danger">
                          <select>
                            <option>Select dropdown</option>
                            <option>With options</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="column is-10-fullhd is-9-widescreen is-8-desktop">
              <div className="box">
                <div className="columns flex-wrap is-multiline">
                  {
                    campaigns.map(campaign =>
                        <CampaignCard campaign={campaign}/>
                    )
                  }
                </div>
              </div>
            </div>

          </div>

        </div>
      </Layout>
  )
}

export async function getServerSideProps(ctx) {
  const campaigns = await findAll()
  return {props: {campaigns}}
}