import {findAll} from '../../../db/campaign-repository'

export default (req, res) =>
    findAll()
        .then((rows) => {
            res.statusCode = 200
            res.json(rows)
        })
        .catch((err) => {
            console.log(err)
            res.statusCode = 500
        })
        .then(() =>
            res.end()
        )
