# TODOs
- Consolidate styles into css classes
- Fix styling of cards (min-width?)
- Implement auth
- Other pages?
- Other tables (only campaigns at present...)
- Explore connection pooling with Next serverless architecture