import knex from 'knex'

export default knex({
    client: 'sqlite3',
    connection: {
        filename: './dev.sqlite3'
    },
    useNullAsDefault: true // Default values are not supported by knex + sqlite
})