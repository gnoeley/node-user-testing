export interface Campaign {
    id: number;
    createdAt: number;
    updatedAt: number;
    title: string;
    slug: string;
    description: string;
    startTime: number;
    endTime: number;
    testStartTime: number;
    testEndTime: number;
}