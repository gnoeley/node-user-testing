import db from './connection'
import {Campaign} from './models'

export const findAll = () => db('campaigns').select<Campaign[]>()