export async function up(knex) {
    return knex.schema
        .createTable('campaigns', function (table) {
            table.increments('id')
            table.timestamp('create_at').defaultTo(knex.fn.now())
            table.timestamp('updated_at').defaultTo(knex.fn.now())
            table.string('title')
            table.string('slug').unique()
            table.text('description').nullable()
            table.dateTime('start_time').nullable()
            table.dateTime('end_time').nullable()
            table.dateTime('test_start_time').nullable()
            table.dateTime('test_end_time').nullable()
        })
}

export async function down(knex) {
    return knex.schema.dropTableIfExists('campaigns')
}

