import Faker from "faker"

function createFakeCampaigns(count){
  const range = Array(count).keys()
  return Array.from(range)
      .map(i => {
        return {
          "id": i,
          "create_at": null,
          "updated_at": null,
          "title": Faker.commerce.productName(),
          "slug": Faker.lorem.slug(),
          "description": Faker.lorem.sentences(),
          "start_time": Faker.date.past(0.1, new Date().toISOString()),
          "end_time": Faker.date.future(0.1, new Date().toISOString()),
          "test_start_time": Faker.date.past(0.1, new Date().toISOString()),
          "test_end_time": Faker.date.future(0.1, new Date().toISOString()),
        }
      })
}

exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("campaigns").del();

  // Inserts seed entries
  await knex("campaigns").insert(createFakeCampaigns(20));
};
