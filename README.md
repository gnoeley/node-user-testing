# JS/Node User Testing 

Based on the php version of the project here https://gitlab.com/tux-support/user-testing.

Currently implemented:
- `/campaign` page with (mostly) correct styling
- `/api/campaigns` endpoint to return Campaign records from db
- In-memory dev db (no staging/production db configured yet)
- DB migrations and seed files for table `campaigns`. Not all columns created yet for brevity.

## How to run
1. Install node (recommend using the `nvm` tool to manage node versions) of version `>=14.x`
2. Run `npm install`
3. Start the app locally with `npm run dev`

## Info
This project is based on the [Next.js](https://nextjs.org/) framework using the [KnexJS](http://knexjs.org/) library for SQL query building and migrations.

If you want to dig into the code, the best place to start is the `/pages/campaign/index.js` file which contains the top-level component for the 'Campaigns' page.