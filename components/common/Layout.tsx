import React, {PropsWithChildren} from 'react'
import styles from '../../styles/Layout.module.css'
import NavBar from './NavBar'

export default function Layout({children}: PropsWithChildren<{}>) {
    return (
        <div className={styles.container}>
            <NavBar/>
            {children}
        </div>
    )
}