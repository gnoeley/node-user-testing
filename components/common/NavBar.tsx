import {FunctionComponent} from 'react'
import {useRouter} from 'next/router'
import '../../styles/Navbar.module.css'

export default function NavBar() {
    return (
        <nav className="navbar has-shadow is-primary"
             role="navigation"
             aria-label="main navigation">
            <div className="navbar-brand">
                <a href="/"
                   className="navbar-item">Shoutout!</a>

                <a role="button" className="navbar-burger burger" aria-label="menu"
                   aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                </a>
            </div>

            <div id="top-navbar" className="navbar-menu">
                <div className="navbar-start">

                    <NavItem href="/" label="Home" icon={HomeIcon}/>
                    <NavItem href="/campaign" label="Campaigns" icon={CampaignsIcon}/>
                    <NavItem href="/organisation" label="Organisations" icon={OrganisationsIcon}/>

                    <div className="navbar-item has-dropdown is-hoverable">
                        <a className="navbar-link">
                            More
                        </a>

                        <div className="navbar-dropdown">
                            <a className="navbar-item" href="/about">
                                About
                            </a>
                            <a className="navbar-item" href="/credits">
                                Credits
                            </a>
                        </div>
                    </div>
                </div>

                <div className="navbar-end">
                    {/*@if(Auth::guest())*/}
                    <a className="navbar-item "
                       href="/login">Login</a>
                    <a className="navbar-item "
                       href="/register">Register</a>
                    {/*@else*/}
                    {/*<div className="navbar-item has-dropdown is-hoverable">*/}
                    {/*  <a className="navbar-link"*/}
                    {/*     href="#">{{ Auth::user()->name }}</a>*/}

                    {/*  <div className="navbar-dropdown">*/}
                    {/*    <a className="navbar-item"*/}
                    {/*       href="{{ route('logout') }}"*/}
                    {/*       onclick="event.preventDefault();document.getElementById('logout-form').submit();">*/}
                    {/*      Logout*/}
                    {/*    </a>*/}

                    {/*    <form id="logout-form"*/}
                    {/*          action="{{ route('logout') }}"*/}
                    {/*          method="POST" style="display: none;">*/}
                    {/*      {{ csrf_field() }}*/}
                    {/*    </form>*/}
                    {/*  </div>*/}
                    {/*</div>*/}
                    {/*@endif*/}
                </div>
            </div>
        </nav>
    )
}

type NavItemProps = {
    href: string,
    label: string,
    icon: FunctionComponent
}
function NavItem({href, label, icon: Icon}: NavItemProps) {
    const router = useRouter()
    const isActive = router.pathname === href
    return (
        <a className={`navbar-item${isActive ? " is-active" : ""}`}
           href={href}>
        <span className="icon is-medium">
          <Icon/>
        </span>
            {label}
        </a>
    )
}

function HomeIcon() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white ">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                  d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"/>
        </svg>
    )
}

function CampaignsIcon() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                  d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z"/>
        </svg>
    )
}

function OrganisationsIcon() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="white">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                  d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"/>
        </svg>
    )
}