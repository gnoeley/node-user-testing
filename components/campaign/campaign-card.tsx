import React from 'react'
import {Campaign} from '../../db/models'

type CampaignCardProps = {
    campaign: Campaign
}

export default function CampaignCard({campaign}: CampaignCardProps) {
    return (
        <div
            className="column is-3-desktop is-2-widescreen is-half-tablet {{ $tag }}">
            <div className="card card-fullheight">
                <div className="card-header">
                    <p className="card-header-title is-centered">
                        <a href="https://bulma.io/">{campaign.title}</a>
                    </p>
                </div>
                <div className="card-image">
                    <figure className="image is-4by1">
                        <img src="/logos/bulma-logo.png"
                             alt="Megaphone, people and a computer"/>
                    </figure>
                </div>
                <div className="card-content">
                    <p className="content">{campaign.description}</p>
                    <p>
                        <span className="tag">
                            {
                                // removed this... $tag = $arraykeys[rand(0,3)];
                                // {{$campaigntags['tag-software'][$tag]['display']}}
                            }
                        </span>
                    </p>
                </div>
                <footer className="card-footer">
                    <a href="https://bulma.io/"
                       className="card-footer-item has-text-centered">
                        <span className="icon is-medium">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 fill="none" viewBox="0 0 24 24"
                                 stroke="currentColor">
                                <path strokeLinecap="round"
                                      strokeLinejoin="round"
                                      strokeWidth={2}
                                      d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"/>
                            </svg>
                        </span>
                        Website
                    </a>
                </footer>
            </div>
        </div>
    )
}